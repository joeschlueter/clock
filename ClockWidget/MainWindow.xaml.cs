﻿using ClassLibrary1;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace ClockWidget
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public static int position1;
        public static int position2;
        public static int position3;
        public static int position4;
        public static int position5;
        public static int position6;

        public MainWindow()
        {
            InitializeComponent();
            DispatcherTimer timer = new DispatcherTimer();
            timer.Interval = TimeSpan.FromSeconds(0.1);
            timer.Tick += RunClock;
            timer.Start();
        }

        private void RunClock(object sender, EventArgs e)
        {
            DateTime now = DateTime.Now;
            //now = new DateTime(2021, 2, 26, 10, 29, 0);
            int hour = now.Hour;
            int minute = now.Minute;
            int second = now.Second;
            int tempPosition1 = (GetHour1PositionFromTime(hour) * -30);
            int tempPosition2 = GetHour2PositionFromTime(hour) * -30;
            int tempPosition3 = GetMinute1PositionFromTime(minute) * -30;
            int tempPosition4 = GetMinute2PositionFromTime(minute) * -30;
            int tempPosition5 = GetSecond1PositionFromTime(second) * -30;
            int tempPosition6 = GetSecond2PositionFromTime(second) * -30;

            if(tempPosition1 != position1)
            {
                int lastPosition = position1;
                position1 = tempPosition1;
                TranslateTransform transPosition1 = new TranslateTransform();
                Hour1.RenderTransform = transPosition1;
                DoubleAnimation animatePosition1 = new DoubleAnimation(lastPosition, position1, TimeSpan.FromSeconds(1));
                transPosition1.BeginAnimation(TranslateTransform.YProperty, animatePosition1);
            }
            
            if(tempPosition2 != position2)
            {
                int lastPosition = position2;
                position2 = tempPosition2;
                TranslateTransform transPosition2 = new TranslateTransform();
                Hour2.RenderTransform = transPosition2;
                DoubleAnimation animatePosition2 = new DoubleAnimation(lastPosition, position2, TimeSpan.FromSeconds(1));
                transPosition2.BeginAnimation(TranslateTransform.YProperty, animatePosition2);
            }
            
            if(tempPosition3 != position3)
            {
                int lastPosition = position3;
                position3 = tempPosition3;
                TranslateTransform transPosition3 = new TranslateTransform();
                Minute1.RenderTransform = transPosition3;
                DoubleAnimation animatePosition3 = new DoubleAnimation(lastPosition, position3, TimeSpan.FromSeconds(1));
                transPosition3.BeginAnimation(TranslateTransform.YProperty, animatePosition3);
            }
            
            if(tempPosition4 != position4)
            {
                int lastPosition = position4;
                position4 = tempPosition4;
                TranslateTransform transPosition4 = new TranslateTransform();
                Minute2.RenderTransform = transPosition4;
                DoubleAnimation animatePosition4 = new DoubleAnimation(lastPosition, position4, TimeSpan.FromSeconds(1));
                transPosition4.BeginAnimation(TranslateTransform.YProperty, animatePosition4);
            }

            if (tempPosition5 != position5)
            {
                int lastPosition = position5;
                position5 = tempPosition5;
                TranslateTransform transPosition5 = new TranslateTransform();
                Second1.RenderTransform = transPosition5;
                DoubleAnimation animatePosition5 = new DoubleAnimation(lastPosition, position5, TimeSpan.FromSeconds(0.5));
                transPosition5.BeginAnimation(TranslateTransform.YProperty, animatePosition5);
            }

            if (tempPosition6 != position6)
            {
                int lastPosition = position6;
                position6 = tempPosition6;
                TranslateTransform transPosition6 = new TranslateTransform();
                Second2.RenderTransform = transPosition6;
                DoubleAnimation animatePosition6 = new DoubleAnimation(lastPosition, position6, TimeSpan.FromSeconds(0.5));
                transPosition6.BeginAnimation(TranslateTransform.YProperty, animatePosition6);
            }
        }

        private int GetHour1PositionFromTime(int hour)
        {
            if (hour > 9 && hour <= 12)
                return 1;
            return 0;
        }

        private int GetHour2PositionFromTime(int hour)
        {
            if (hour > 12)
                return hour - 12;
            switch (hour)
            {
                case 10:
                    return 0;
                case 11:
                    return 1;
                case 12:
                    return 2;
                default:
                    return hour;
            }
        }

        private int GetMinute1PositionFromTime(int minute)
        {
            if (minute < 10)
                return 0;
            var m = minute.ToString().ToArray();
            return Convert.ToInt32(m[0].ToString());
        }

        private int GetMinute2PositionFromTime(int minute)
        {
            if (minute < 10)
                return minute;
            var m = minute.ToString().ToArray();
            return Convert.ToInt32(m[1].ToString());
        }

        private int GetSecond1PositionFromTime(int second)
        {
            if (second < 10)
                return 0;
            var m = second.ToString().ToArray();
            return Convert.ToInt32(m[0].ToString());
        }

        private int GetSecond2PositionFromTime(int second)
        {
            if (second < 10)
                return second;
            var m = second.ToString().ToArray();
            return Convert.ToInt32(m[1].ToString());
        }

        private void Window_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.LeftButton == MouseButtonState.Pressed)
                this.DragMove();
        }
    }
}
